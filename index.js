const minimist = require('minimist')
const path = require('path')
const fs = require('fs')

const capitalizeFirstLetter = (string) => string.charAt(0).toUpperCase() + string.slice(1)

module.exports = () => {
  const args = minimist(process.argv.slice(2))
  const cwd = process.cwd()
  
  if (!args || !args._.length) {
    console.error(`Input filename required. Ex: iconfont-css-to-csharp style.css`)
    process.exit(-1)
  }

  const filename = args._[0]
  const filepath = path.resolve(cwd, filename)
  const outputPath = path.resolve(cwd, `${filename.split('.')[0]}.cs`)

  const content = fs.readFileSync(filepath, 'utf8')
  const regex = new RegExp(/\.icon-(?:([\w|-]+)).+\n.+content:\s("\\\w+");\n}/gm)

  // Write to File
  const header = `
  using System;
  using System.Reflection;
  namespace Iconfont
  {
    public static class Iconfont
    {
  `
  const footer = `
    }
  }
  `

  const body = []
  while ((m = regex.exec(content)) !== null) {
    if (m.index === regex.lastIndex) {
      regex.lastIndex++;
    }

    const value = m[2].includes('\\f') ? m[2].replace('\\f', '\\uf') : m[2]
    let varname = capitalizeFirstLetter(m[1])
    if (varname.includes('-')) {
      varname = varname.split('-').reduce((accumulator, currentValue) => accumulator += capitalizeFirstLetter(currentValue))
    }

    body.push(`\t\tpublic static string ${varname} = ${value};\n`)
  }
  body.sort()

  fs.writeFileSync(outputPath, header + body.join('') + footer)
  console.log(`SUCCESS: Wrote declaration to: ${outputPath}`)
  
  process.exit(0)
}